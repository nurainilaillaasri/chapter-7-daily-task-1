import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';
import Header from './components/headerComponents/Header'
import MainLayout from "./layouts/MainLayout";
import Button from "./components/buttonComponents/Button";

function State() {
  return (
    <div className="State mt-5">
      <Header title="Button Components" />
      
      <MainLayout>
        <Button variant="blue">
          Click
        </Button>

        <Button variant="secondary">
          Click
        </Button>

        <Button variant="black">
          Click
        </Button>
      </MainLayout>
    </div>
  );
}

export default State;
