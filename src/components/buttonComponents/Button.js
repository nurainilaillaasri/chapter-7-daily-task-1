import React from "react";
import classNames from "classnames"
import { PropTypes } from "prop-types"
import style from "./Button.module.css"

const backgroundColor = {
  blue: "#03076F",
  secondary: "#ffffff",
  black: "#000000",
}

const textColor = {
  blue: "#92E8EC",
  secondary: "#000000",
  black: "#ffffff",
}

const Button = ({ children, variant, onClick, className, ...props }) => {
  const styles = {
    backgroundColor: backgroundColor[variant] || backgroundColor.blue,
    color: textColor[variant] || textColor.blue,
  };

  return (
    <button style={styles} {...props} onClick={onClick} className={classNames(style.Button, className)}>
      {children}
    </button>
  )
}

Button.propTypes = {
  variant: PropTypes.oneOf([
    "blue",
    "secondary",
    "black"
  ]),
  onClick: PropTypes.func,
}

Button.defaultProps = {
  variant: "blue",
  onClick: () => {},
}

export default Button;

