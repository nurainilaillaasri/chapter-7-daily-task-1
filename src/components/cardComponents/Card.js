import React from "react";
import classNames from "classnames"
import Button from "../buttonComponents/Button";
import style from "./Card.module.css"

const Card = (props) => {
  const { title, description, imgSrc, imgAlt, btnText, btnHref, className } = props;

  return (
    // <div className="card" style={{ width: "18rem" }}>
    <div className={classNames(style.Card, className)}>
      <img src={imgSrc} className="card-img-top" alt={imgAlt} />
      <div className="card-body">
        <h5 className="card-title">{title}</h5>
        <p className="card-text">{description}</p>
        <a href={btnHref}>
          <Button variant="blue" className="btn btn-primary">
            {btnText}
          </Button>
        </a>
      </div>
    </div>
  )
}

export default Card;
