import Card from './components/cardComponents/Card';
import Header from './components/headerComponents/Header';
import Title from './components/titleComponents/HeadingTitle';
import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';

function App() {
  return (
    <div className="App">
      <Title title="React" />

      <Header
        title="Hello FSW13"
      />

      <Card
        title="Hello"
        description="Lorem ipsum dolor sit amet"
        btnText="Go Somewhere"
        btnHref="https://google.com"
        imgSrc="https://placeimg.com/320/240/any"
        imgAlt="Hello"
      />
    </div>
  );
}

export default App;
